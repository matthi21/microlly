from __future__ import unicode_literals
from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.contrib.auth import login, logout



class Post_Plus(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    slug = models.SlugField(default='a-slug')
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    post_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def preview(self):
        return self.body[:100]+'...'

    def _get_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        num = 1
        while Post_Plus.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        self.slug = self._get_slug()
        super().save(*args, **kwargs)

    class Meta():
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'