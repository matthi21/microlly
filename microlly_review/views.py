from django.shortcuts import render, get_object_or_404, redirect
from microlly_review.models import Post_Plus
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from microlly_review import forms
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, logout


def sign_up(request):
    if request.method == 'POST':
        form = forms.SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('microlly_review:index')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def log_in(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('microlly_review:index')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})


def log_out(request):
    if request.method == 'POST':
        logout(request)
        return redirect('microlly_review:index')
    else:
        return redirect('microlly_review:index')


def index(request):
    posts_list = Post_Plus.objects.all().order_by('-post_date')
    paginator = Paginator(posts_list, 9)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request, 'index.html', {'last_posts': posts})


def plus(request, slug, pk):
    post = get_object_or_404(Post_Plus, slug=slug, pk=pk)
    return render(request, 'plus.html', {'post': post})

def post_list(request, author):

    author = get_object_or_404(User, username=author)
    posts_list = Post_Plus.objects.filter(author=author)
    print(posts_list)
    paginator = Paginator(posts_list, 9)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    context = {
        "author": author,
        "list_posts_user": posts,
    }
    return render(request, 'post_list.html', context)


def delete(request, slug, pk):
    post = get_object_or_404(Post_Plus, pk=pk)
    if request.user != post.author:
        return redirect('microlly_review:index')
    if request.method == 'POST':
        post.delete()
        return redirect('microlly_review:index')
    return render(request, 'login.html')

@login_required(login_url="/microlly_review/login/")
def creation_post(request):
    if request.method == 'POST':
        form = forms.Creation(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('microlly_review:index')
    else:
        form = forms.Creation()
    return render(request, 'creation_post.html', {'form': form})


@login_required(login_url="/microlly_review/login/")
def edition_post(request, slug, pk):
    post = get_object_or_404(Post_Plus, slug=slug, pk=pk)
    form = forms.Edition(request.POST or None, instance=post)
    if request.user != post.author:
        return redirect('microlly_review:index')
    if request.method == 'POST':
        if form.is_valid():
            post.save()
            return redirect('microlly_review:index')
    context = {
        "form": form,
        "post": post,
    }
    return render(request, 'edition_post.html', context)


