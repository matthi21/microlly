from django.apps import AppConfig


class MicrollyReviewConfig(AppConfig):
    name = 'microlly_review'
