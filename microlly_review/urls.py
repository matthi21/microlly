from django.urls import path
from microlly_review import views

app_name = 'microlly_review'

urlpatterns = [
    path('signup/', views.sign_up, name='signup'),
    path('login/', views.log_in, name='login'),
    path('logout/', views.log_out, name='logout'),
    path('', views.index, name='index'),
    path('plus/<slug:slug>/<int:pk>', views.plus, name='plus'),
    path('edit/<slug:slug>/<int:pk>', views.edition_post, name='edition'),
    path('delete/<slug:slug>/<int:pk>', views.delete, name='delete'),
    path('create/', views.creation_post, name='creation'),
    path('post_user/<str:author>',
         views.post_list, name='post_list'),
    
]

