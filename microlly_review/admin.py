from django.contrib import admin
from microlly_review.models import *


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'post_date', 'edit_date',)
    search_fields = ('title',)
    list_filter = ('post_date', 'edit_date', 'author')

admin.site.register(Post_Plus, PostAdmin)
